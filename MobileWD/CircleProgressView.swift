//
//  CircleProgressView.swift
//  CustomUITest
//
//  Created by Mattia Pagini on 23/01/16.
//  Copyright © 2016 Mattia Pagini. All rights reserved.
//

import UIKit

/**
 View per gestire qualsiasi cosa abbia bisogno di 3 step per essere completata
 potrebbe essere migliorata permettendo l'inserimento di un numero dinamico di step
 da Interface Builder
 */
@IBDesignable class CircleProgressView: UIView {

    //tiene lo step corrente della view, inizialmente impostato a 1
    //il primo cerchio sarà sempre selezionato
    var currentState : Int = 1
    
    //shape dei vari cerchi
    var leftCircleLayer : CAShapeLayer!
    var middleCircleLayer : CAShapeLayer!
    var rightCircleLayer : CAShapeLayer!
    
    //linee di congiunzione
    var leftLineLayer : CAShapeLayer!
    var rightLineLayer : CAShapeLayer!
    
    //variabili statiche di disegno
    var circleRadius : CGFloat!
    var middleCircleCenter : CGPoint!
    var leftCircleCenter : CGPoint!
    var rightCircleCenter : CGPoint!
    
    //variabile per il colore di riempimento dei cerchi
    @IBInspectable var fillColor : UIColor = UIColor.blueColor() {
        didSet {
            initialDrawing()
        }
    }

    //variabile per il colore del bordo dei cerchi
    @IBInspectable var strokeColor : UIColor = UIColor.blueColor() {
        didSet {
            initialDrawing()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeStaticVariable(frame)
        initialDrawing()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeStaticVariable(frame)
        initialDrawing()
    }
    
    //permette di visualizzare il tutto tramite l'interfaccia
    override func prepareForInterfaceBuilder() {
        initializeStaticVariable(frame)
        initialDrawing()
    }

    
    //funzione iniziale per posizionare i vari elementi da aggiungere al layer della view
    private func initialDrawing() {
        
        //shape layer per i cerchi
        leftCircleLayer = CAShapeLayer()
        middleCircleLayer = CAShapeLayer()
        rightCircleLayer = CAShapeLayer()
        
        //shape layer per le linee che connettono i cerchi
        leftLineLayer = CAShapeLayer()
        rightLineLayer = CAShapeLayer()
        
        //linea di congiunzione tra il cerchio di sinistra e quello centrale        
        let leftMiddlePath = UIBezierPath()
        leftMiddlePath.moveToPoint(CGPoint(x: leftCircleCenter.x + circleRadius, y: leftCircleCenter.y))
        leftMiddlePath.addLineToPoint(CGPoint(x: middleCircleCenter.x - circleRadius, y: middleCircleCenter.y))
        
        //linea di congiunzione tra il cerchio centrale e quello di destra
        let middleRightPath = UIBezierPath()
        middleRightPath.moveToPoint(CGPoint(x: middleCircleCenter.x + circleRadius, y: middleCircleCenter.y))
        middleRightPath.addLineToPoint(CGPoint(x: rightCircleCenter.x - circleRadius, y: rightCircleCenter.y))
        
        //assegniamo il path della linea al nostro CAShapeLayer perchè ne assuma la stessa forma
        leftLineLayer.path = leftMiddlePath.CGPath
        leftLineLayer.strokeColor = strokeColor.CGColor
        leftLineLayer.lineWidth = 2
        //previene il disegno iniziale della linea
        leftLineLayer.strokeEnd = 0
        
        rightLineLayer.path = middleRightPath.CGPath
        rightLineLayer.strokeColor = strokeColor.CGColor
        rightLineLayer.lineWidth = 2
        //previene il disegno iniziale della linea
        rightLineLayer.strokeEnd = 0
        
        //aggiungiamo i sotto layer alla view
        layer.addSublayer(leftLineLayer)
        layer.addSublayer(rightLineLayer)
        
        
        //creiamo i cerchi usando le posizioni e il raggio trovati precendentemente
        let middleCircle = UIBezierPath(arcCenter: middleCircleCenter, radius: circleRadius, startAngle: 0, endAngle: CGFloat(M_PI * 2), clockwise: true)
        
        let leftCircle = UIBezierPath(arcCenter: leftCircleCenter, radius: circleRadius, startAngle: 0, endAngle: CGFloat(M_PI * 2), clockwise: true)
        
        let rightCircle = UIBezierPath(arcCenter: rightCircleCenter, radius: circleRadius, startAngle: 0, endAngle: CGFloat(M_PI * 2), clockwise: true)
        
        //assegniamo il path dei cerchi ai vari CAShapeLayer che abbiamo creato prima
        leftCircleLayer.path = leftCircle.CGPath
        leftCircleLayer.fillColor = fillColor.CGColor
        leftCircleLayer.strokeColor = strokeColor.CGColor
        leftCircleLayer.lineWidth = 2
        
        middleCircleLayer.path = middleCircle.CGPath
        middleCircleLayer.fillColor = UIColor.clearColor().CGColor
        middleCircleLayer.strokeColor = strokeColor.CGColor
        middleCircleLayer.lineWidth = 2
 
        rightCircleLayer.path = rightCircle.CGPath
        rightCircleLayer.fillColor = UIColor.clearColor().CGColor
        rightCircleLayer.strokeColor = strokeColor.CGColor
        rightCircleLayer.lineWidth = 2
        
        //aggiungo i miei CAShapeLayer come sottolayer della view
        layer.addSublayer(leftCircleLayer)
        layer.addSublayer(middleCircleLayer)
        layer.addSublayer(rightCircleLayer)
        
    }
    
    //Metodo iniziale per calcolare i posizionamenti corretti dei cerchi e le dimensioni degli stessi
    private func initializeStaticVariable(frame: CGRect){
        
        //calcoliamo il raggio dei nostri cerchi come 1/4 dell'altezza della view
        circleRadius = frame.size.height / 4
        
        //posizioniamo i centri dei nostri cerchi in modo da averne 3 sulla stessa linea e alla stessa distanza
        middleCircleCenter = CGPoint(x: frame.size.width / 2, y: frame.size.height / 2)
        leftCircleCenter = CGPoint(x: circleRadius * 2, y: frame.size.height / 2)
        rightCircleCenter = CGPoint(x: frame.size.width - circleRadius * 2, y: frame.size.height / 2)
        
    }
    

    //--------------------- FUNZIONI DI ANIMAZIONE -----------------------
    
    private enum Step {
        case FIRST_TO_SECOND
        case SECOND_TO_FIRST
        case SECOND_TO_THIRD
        case THIRD_TO_SECOND
    }
    
    //procede al prossimo step (se possibile)
    func nextStep() {
        switch currentState {
        case 1 : animateStep(Step.FIRST_TO_SECOND); break
        case 2 : animateStep(Step.SECOND_TO_THIRD); break
        default : ()
        }
        
        //non posso andare oltre il 3° cerchio
        if currentState < 3 {
            currentState++
        }
    }
    
    //procede allo steo precedente (se possibile)
    func prevStep() {
        switch currentState {
        case 2 : animateStep(Step.SECOND_TO_FIRST); break
        case 3 : animateStep(Step.THIRD_TO_SECOND); break
        default : ()
        }
        
        //non posso andare sotto il 1° cerchio
        if currentState > 1 {
            currentState--
        }
    }
    
    //animazione della linea che collega i cerchi al cambio di step, alla fine dell'animazione
    //lancia automaticamente l'animazione sul riempimento del cerchio
    private func animateStep(currentStep : Step) {
        
        CATransaction.begin()
        //animiamo la property strokeEnd
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        //la durata la lasciamo fissa a 1
        animation.duration = 0.6
        //in base al tipo di salto che dobbiamo fare impostiamo una determinato valore
        switch currentStep {
        case .FIRST_TO_SECOND, .SECOND_TO_THIRD:
            animation.fromValue = 0
            animation.toValue = 1
            break
        case .SECOND_TO_FIRST, .THIRD_TO_SECOND:
            animation.fromValue = 1
            animation.toValue = 0
            break
        }
        //faccio in modo che finita l'animazione la modifica rimanga
        animation.fillMode = kCAFillModeForwards
        animation.removedOnCompletion = false
        //curva d'animazione ease-out
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        
        //blocco che viene eseguito quando l'animazione viene completata
        CATransaction.setCompletionBlock({
            self.animateCircleFilling(currentStep)
        })
        
        //seleziono la linea che deve eseguire la transizione
        switch currentStep {
        case .FIRST_TO_SECOND, .SECOND_TO_FIRST:
            leftLineLayer.addAnimation(animation, forKey: animation.keyPath)
            break
        case .SECOND_TO_THIRD, .THIRD_TO_SECOND:
            rightLineLayer.addAnimation(animation, forKey: animation.keyPath)
            break
        }

        //lancio l'animazione
        CATransaction.commit()
    }
    
    //funzione per animare il riempimento dei cerchi
    private func animateCircleFilling(currentStep: Step){
        
        //creo una animazione per la proprietà fillColor dei cerchi
        let animation = CABasicAnimation(keyPath: "fillColor")
        animation.duration = 0.2
        switch currentStep {
        case .FIRST_TO_SECOND, .SECOND_TO_THIRD:
            animation.fromValue = UIColor.clearColor().CGColor
            animation.toValue = fillColor.CGColor
            break
        case .SECOND_TO_FIRST, .THIRD_TO_SECOND:
            animation.fromValue = fillColor.CGColor
            animation.toValue = UIColor.clearColor().CGColor
            break
        }
        //faccio in modo che finita l'animazione la modifica rimanga
        animation.fillMode = kCAFillModeForwards
        animation.removedOnCompletion = false
        //curva d'animazione
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        
        //seleziono il cerchio che deve eseguire la transizione
        switch currentStep {
        case .FIRST_TO_SECOND, .SECOND_TO_FIRST:
            middleCircleLayer.addAnimation(animation, forKey: animation.keyPath)
            break
        case .SECOND_TO_THIRD, .THIRD_TO_SECOND:
            rightCircleLayer.addAnimation(animation, forKey: animation.keyPath)
            break
        }
        
    }

}
