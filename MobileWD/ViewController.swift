//
//  ViewController.swift
//  MobileWD
//
//  Created by Mattia Pagini on 26/01/16.
//  Copyright © 2016 Mattia Pagini. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var stateView: CircleProgressView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var prevButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //bottone indietro
    @IBAction func prevButtonTouched(sender: UIButton) {
        stateView.prevStep()
    }
    
    //bottone avanti
    @IBAction func nextButtonTouched(sender: UIButton) {
        stateView.nextStep()
    }
    

}

